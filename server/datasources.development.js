'use strict';
module.exports = {
  db: {
    url: process.env.POSTGRES_CONNECTION_STRING,
    name: 'db',
    connector: 'postgresql',
  },
};

