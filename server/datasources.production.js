'use strict';
let dbConfig = {
  name: 'db',
  connector: 'memory',
};
if (process.env.POSTGRES_CONNECTION_STRING) {
  dbConfig = {
    url: process.env.POSTGRES_CONNECTION_STRING,
    name: 'db',
    connector: 'postgresql',
  };
}
module.exports = {
  db: dbConfig,
};

