'use strict';
var assert = require('chai').assert;
var app = require('../server/server');
var request = require('supertest')(app);
var boot = require('loopback-boot');

describe('AppUser', function() {
  before('run boot script', function(done) {
    boot(app, `${__dirname}/../server`, done);
  });
  let user = {
    email: 'user@example.com',
    password: 'foobar',
    emailVerified: true,
  };
  it('should register', function(done) {
    request
      .post('/api/Users')
      .send({email: user.email, password: user.password})
      .end(function(err, res) {
        if (err) { return done(err); }
        assert.equal(res.status, 200);
        assert.ok(res.body);
        assert.equal(res.body.email, user.email);
        done();
      });
  });
  it('should not register with same email', function(done) {
    request
      .post('/api/Users')
      .send({email: user.email, password: user.password})
      .end(function(err, res) {
        if (err) { return done(err); }
        assert.equal(res.status, 422);
        done();
      });
  });
  it('should login', function(done) {
    request
      .post('/api/Users/login')
      .send({email: user.email, password: user.password})
      .end(function(err, res) {
        if (err) { return done(err); }
        assert.equal(res.status, 200);
        assert.ok(res.body);
        assert.isDefined(res.body.id);
        done();
      });
  });
});
